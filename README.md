# AC4-DevOps

### GRUPO:
* Alexandre Solano Correia - 2102532
* Elvis Liberato de Barros - 2102592
* Mateus Rodrigues da Silveira Morais 2102608
* Silvana Araujo de Sousa - 2102593

### BUILD:

##### Clonar repositorio

> git clone https://gitlab.com/alexandre.correia/ac4-devops.git

##### Criar imagem
> docker build -t teste/nginx:1.0 .

##### Criar container
> docker container create -p 8080:80 teste/nginx:1.0


#### Prova que foi testado:
<p align="center">
  <img src="sourceReadme/img01.jpeg" alt="Evidência imagem criada no docker">
  <img src="sourceReadme/img02.jpeg" alt="Evidência container criado no docker">
  <img src="sourceReadme/img03.jpeg" alt="Evidência acessando a porta mapeada do container no navegador">
</p>
